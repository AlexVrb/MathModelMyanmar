#include "country.h"

country::country()
{
    values[ice] = 0;
    values[alpha] = 0;
    values[delta] = 0;
    values[n] = 0;
    values[s] = 0;
}

country::country(const double iceValue,
                 const double alphaValue,
                 const double deltaValue,
                 const double nValue,
                 const double sValue)
{
    values[ice] = iceValue;
    values[alpha] = alphaValue;
    values[delta] = deltaValue;
    values[n] = nValue;
    values[s] = sValue;
}

double country::getValue(valueindex i) const
{
    return values[i];
}
