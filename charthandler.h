#ifndef FORMHANDLER_H
#define FORMHANDLER_H

#include <QLineSeries>
#include <QSlider>
#include <QValueAxis>
#include <QTextBrowser>

#include "example.h"

using namespace QtCharts;


const int tmax = 50;

class ChartHandler
{
public:
    ChartHandler(QTextBrowser* atbDesc);
    ~ChartHandler();

    void drawChart(QSlider* sldN,
                   QSlider* sldDelta,
                   QSlider* sldS,
                   QSlider* sldICE,
                   QSlider* sldAlpha);
    QChart* getChart();

    void loadExample(Example ex);

    void setShowChina(bool afShowChina);

    void setExercise(bool aFlag);

    void setChartTitle(QString astrTitle);

private:

    void drawCountryChart(const country coun, QLineSeries* lsCoun);

    QLineSeries* ls;
    QLineSeries* lsBangladesh;
    QLineSeries* lsChina;

    QChart* chart;

    bool showChina = false;

    Example currExample;

    bool exercise;

    QTextBrowser* tbDesc;
};

#endif // FORMHANDLER_H
