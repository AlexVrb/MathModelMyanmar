#ifndef EXAMPLE_H
#define EXAMPLE_H

#include "country.h"

enum countryindex
{
    enMyanmar = 0,
    enBangladesh,
    enChina,
    countryParameters
};

class Example
{
public:
    Example();

    Example(const country acMyanmar,
            const country acBangladesh,
            const country acChina);

    country getValue(countryindex index) const;

private:

    country values[countryParameters];
};

#endif // EXAMPLE_H
