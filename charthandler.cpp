#include "charthandler.h"

#include <QChart>

#include "productionfunctions.h"

ChartHandler::ChartHandler(QTextBrowser *atbDesc)
{
    tbDesc = atbDesc;
    ls = new QLineSeries();
    lsBangladesh = new QLineSeries();
    lsChina = new QLineSeries();

    ls->setUseOpenGL(true);
    lsBangladesh->setUseOpenGL(true);
    lsChina->setUseOpenGL(true);

    chart = new QChart();
    chart->setTitle("Capital endowment model");

    chart->addSeries(ls);
    chart->addSeries(lsBangladesh);
    if(showChina)
    {
        chart->addSeries(lsChina);
    }

    chart->legend()->hide();

    exercise = false;
}

ChartHandler::~ChartHandler()
{
    if(chart)
    {
        delete chart;
        chart = NULL;
    }
    if(ls)
    {
        delete ls;
        ls = NULL;
    }
    if(lsBangladesh)
    {
        delete lsBangladesh;
        lsBangladesh = NULL;
    }
    if(lsChina)
    {
        delete lsChina;
        lsChina = NULL;
    }
}

void ChartHandler::loadExample(Example ex)
{
    drawCountryChart(ex.getValue(enBangladesh), lsBangladesh);
    if(showChina)
    {
        drawCountryChart(ex.getValue(enChina), lsChina);
    }
    currExample = ex;
}

void ChartHandler::drawChart(QSlider* sldN,
                             QSlider* sldDelta,
                             QSlider* sldS,
                             QSlider* sldICE,
                             QSlider* sldAlpha)
{
    ls->clear();
    chart->removeSeries(ls);

    int t0 = 0;
    double eta = sldICE->value();

    double (*PF)(double, double);
    PF = &productionfunctions::KobbDouglas;

    for(int t = t0; t <= tmax; ++t)
    {
        ls->append(t, eta);

        eta = eta + (double)sldS->value() / 10
                * PF((double)sldAlpha->value() / 10, eta)
                - ((double)sldDelta->value() / 10
                   + (double)sldN->value()) / 10 * eta;
    }

    if(exercise && eta > 10)
    {
        tbDesc->setText("You solved the problem!");
    }

    chart->addSeries(ls);
    chart->createDefaultAxes();
}

QChart* ChartHandler::getChart()
{
    return chart;
}

void ChartHandler::drawCountryChart(
        const country coun,
        QLineSeries* lsCoun)
{
    int t0 = 0;
    double eta = coun.getValue(ice);

    lsCoun->clear();
    chart->removeSeries(lsCoun);

    double (*PF)(double, double);
    PF = &productionfunctions::KobbDouglas;

    for(int t = t0; t <= tmax; ++t)
    {
        lsCoun->append(t, eta);

        eta = eta + (double)coun.getValue(s)
                * PF((double)coun.getValue(alpha), eta)
                - ((double)coun.getValue(delta)
                   + (double)coun.getValue(n)) * eta;
    }

    chart->addSeries(lsCoun);
    chart->createDefaultAxes();
}

void ChartHandler::setShowChina(bool afShowChina)
{
    showChina = afShowChina;
    if(showChina)
    {
        lsChina->show();
        drawCountryChart(currExample.getValue(enChina), lsChina);
    }
    else
    {
        lsChina->hide();
        lsChina->clear();
        chart->removeSeries(lsChina);
        chart->createDefaultAxes();
    }
}

void ChartHandler::setExercise(bool aFlag)
{
    exercise = aFlag;
}

void ChartHandler::setChartTitle(QString astrTitle)
{
    chart->setTitle(astrTitle);
}
