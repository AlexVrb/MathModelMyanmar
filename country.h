#ifndef COUNTRY_H
#define COUNTRY_H

enum valueindex
{
    ice = 0,
    alpha,
    delta,
    n,
    s,
    numParameters
};

class country
{
public:
    country();

    country(const double iceValue,
            const double alphaValue,
            const double deltaValue,
            const double nValue,
            const double sValue);

    double getValue(valueindex i) const;

private:

    double values[numParameters];
};

#endif // COUNTRY_H
