#include "example.h"

Example::Example(const country acMyanmar,
                 const country acBangladesh,
                 const country acChina)
{
  values[enMyanmar] = acMyanmar;
  values[enBangladesh] = acBangladesh;
  values[enChina] = acChina;
}

Example::Example()
{
    country def;
    values[enMyanmar] = def;
    values[enBangladesh] = def;
    values[enChina] = def;
}

country Example::getValue(countryindex index) const
{
    return values[index];
}
