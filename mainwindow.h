#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QGridLayout>

enum lang
{
    English,
    Russian
};

enum class Pars
{
    ICE,
    alpha,
    delta,
    s,
    n
};

class Example;

namespace Ui {
class MainWindow;
}

class ChartHandler;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_sldN_valueChanged(int value);

    void on_sldDelta_valueChanged(int value);

    void on_sldS_valueChanged(int value);

    void on_sldICE_valueChanged(int value);

    void on_sldAlpha_valueChanged(int value);

    void on_actionModel_description_triggered();

    void updateValue(int value, QLabel* lbl);

    void on_cbExamples_activated(const QString &arg1);

    void on_chbShowChina_toggled(bool checked);

    void on_actionEnglish_triggered();

    void on_actionRussian_triggered();

    void on_actionAgriculture_triggered();

    void on_actionIndustry_triggered();

    void on_actionTourism_triggered();

    void on_btnBegin_clicked();

    void on_btnEnd_clicked();

private:

    void triggerChange();

    Ui::MainWindow *ui;

    ChartHandler* handler;

    Example* exAgriculture;
    Example* exIndustry;
    Example* exTourism;

    lang curLang;

    bool exercise;

    Pars lastChanged;
};

#endif // MAINWINDOW_H
