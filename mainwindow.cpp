#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDesktopServices>

#include "charthandler.h"

#include "example.h"

const std::vector<QString> examples =
{
    "Approaching Bangladesh (in agriculture)"
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QMainWindow::centralWidget()->setLayout(ui->mainLayout);

    curLang = English;

    //образцы
    //нач. значение, произв., аморт., рост рес., сбереж.
    country myanAgriculture(4, 1.1, 0.5, 1.07, 0.5);
    country myanIndustry(1, 1.1, 0.5, 1.07, 0.5);
    country myanTourism(1, 1.1, 0.5, 1.07, 0.5);

    country chinaAgriculture(97, 1.125, 0.16, 0.38, 0.5);
    country chinaIndustry(97, 1.125, 0.15, 0.38, 0.5);
    country chinaTourism(97, 1.125, 0.13, 0.38, 0.5);

    country bangAgriculture(14, 1.07, 0.16, 0.3, 0.4);
    country bangIndustry(13, 1.07, 0.15, 0.3, 0.4);
    country bangTourism(2, 1.07, 0.13, 0.3, 0.4);

    exAgriculture = new Example(myanAgriculture, bangAgriculture, chinaAgriculture);
    exIndustry = new Example(myanIndustry, bangIndustry, chinaIndustry);
    exTourism = new Example(myanTourism, bangTourism, chinaTourism);

    handler = new ChartHandler(ui->tbDescAction);

    //дефолты
    ui->sldN->setMaximum(10);
    ui->sldAlpha->setMaximum(5);
    ui->sldICE->setMaximum(300);
    ui->tbDescAction->setText("");

    ui->chartview->setChart(handler->getChart());
    ui->chartview->setRenderHint(QPainter::Antialiasing);

    ui->sldAlpha->setValue(exAgriculture->getValue(enMyanmar).getValue(alpha) * 10);
    ui->sldDelta->setValue(exAgriculture->getValue(enMyanmar).getValue(delta) * 10);
    ui->sldN->setValue(exAgriculture->getValue(enMyanmar).getValue(n) * 10);
    ui->sldS->setValue(exAgriculture->getValue(enMyanmar).getValue(s) * 10);
    ui->sldICE->setValue(exAgriculture->getValue(enMyanmar).getValue(ice));

    updateValue(ui->sldN->value(), ui->lblValueN);
    updateValue(ui->sldDelta->value(), ui->lblValueDelta);

    //формирование меню
    for(unsigned i = 0; i < examples.size(); ++i)
    {
        ui->cbExamples->insertItem(i, examples[i]);
    }

    exercise = false;
    on_actionEnglish_triggered();

    //отрисовка
    handler->drawChart(ui->sldN,
                       ui->sldDelta,
                       ui->sldS,
                       ui->sldICE,
                       ui->sldAlpha);
    handler->loadExample(*exAgriculture);
    ui->chartview->update();
}

MainWindow::~MainWindow()
{
    delete ui;

    if(handler)
    {
        delete handler;
        handler = NULL;
    }
    if(exAgriculture)
    {
        delete exAgriculture;
        exAgriculture = NULL;
    }
    if(exIndustry)
    {
        delete exIndustry;
        exIndustry = NULL;
    }
}

void MainWindow::updateValue(int value, QLabel* lbl)
{
    double dvalue = (double)value / 10;
    QString str = QString::number(dvalue, 'g', 2);
    lbl->setText(str);
}

void MainWindow::on_sldN_valueChanged(int value)
{
    updateValue(value, ui->lblValueN);
    handler->drawChart(ui->sldN,
                       ui->sldDelta,
                       ui->sldS,
                       ui->sldICE,
                       ui->sldAlpha);
    if(exercise)
    {
        return;
    }
    if(curLang == Russian)
    {
        ui->tbDescAction->setText("Высокий темп роста трудовых ресурсов приводит к большим расходам на оплату труда, что способно быстро истощить бюджет.");
    }
    else
    {
        ui->tbDescAction->setText("Fast growth of resources leads to big expenses on wages, which can empty the budget very fast.");
    }
    lastChanged = Pars::n;
}

void MainWindow::on_sldDelta_valueChanged(int value)
{
    updateValue(value, ui->lblValueDelta);
    handler->drawChart(ui->sldN,
                       ui->sldDelta,
                       ui->sldS,
                       ui->sldICE,
                       ui->sldAlpha);
    if(exercise)
    {
        return;
    }
    if(curLang == Russian)
    {
        ui->tbDescAction->setText("Норма амортизации показывает долю прибыли, идущую на поддержание используемого оборудования. Высокие значения отрицательно влияют на рост капитала.");
    }
    else
    {
        ui->tbDescAction->setText("Amortization quota shows the part of the income that is used for supporting the equipment in the working state. High values negatively impact the capital growth.");
    }
    lastChanged = Pars::delta;
}

void MainWindow::on_sldS_valueChanged(int value)
{
    updateValue(value, ui->lblValueS);
    handler->drawChart(ui->sldN,
                       ui->sldDelta,
                       ui->sldS,
                       ui->sldICE,
                       ui->sldAlpha);
    if(exercise)
    {
        return;
    }
    if(curLang == Russian)
    {
        ui->tbDescAction->setText("Норма сбережения показывает долю прибыли, идущую на расширение бизнеса. Высокие значения оказывают положительный эффект на рост капитала.");
    }
    else
    {
        ui->tbDescAction->setText("Saving ratio shows the part of income that is used for expanding the business. High values positively impact the capital growth.");
    }
    lastChanged = Pars::s;
}

void MainWindow::on_sldICE_valueChanged(int value)
{
    ui->lblValueICE->setNum(value);
    handler->drawChart(ui->sldN,
                       ui->sldDelta,
                       ui->sldS,
                       ui->sldICE,
                       ui->sldAlpha);
    if(exercise)
    {
        return;
    }
    if(curLang == Russian)
    {
        ui->tbDescAction->setText("Начальная капиталовооруженность отображает стартовую точку отсчета. Также высокие значения оказывают незначительный положительный эффект на скорость роста капитала.");
    }
    else
    {
        ui->tbDescAction->setText("Initial capital endowment defines the starting evaluation point. Also high values have slight positive impact on capital growth speed.");
    }
    lastChanged = Pars::ICE;
}

void MainWindow::on_sldAlpha_valueChanged(int value)
{
    updateValue(value, ui->lblValueAlpha);
    handler->drawChart(ui->sldN,
                       ui->sldDelta,
                       ui->sldS,
                       ui->sldICE,
                       ui->sldAlpha);
    if(exercise)
    {
        return;
    }
    if(curLang == Russian)
    {
        ui->tbDescAction->setText("Производительность оказывает сильный положительный эффект на рост капитала.");
    }
    else
    {
        ui->tbDescAction->setText("Productivity has heavy positive impact on capital growth.");
    }
    lastChanged = Pars::alpha;
}

void MainWindow::on_actionModel_description_triggered()
{
    QFile HelpFile(":/docs/Theory.odt");
    QString strPath = QStandardPaths::writableLocation(QStandardPaths::TempLocation).append("/Theory.odt");
    HelpFile.copy(strPath);
    QDesktopServices::openUrl(QUrl::fromLocalFile(strPath));
}

void MainWindow::on_cbExamples_activated(const QString &arg1)
{
    if(exercise)
    {
        return;
    }

    if(arg1 == examples[0])
    {
        //приближение к Бангладешу
        handler->loadExample(*exAgriculture);

        //ставим значения
        ui->sldAlpha->setValue(5);
        ui->sldDelta->setValue(2);
        ui->sldICE->setValue(4);
        ui->sldN->setValue(4);
        ui->sldS->setValue(8);

        if(curLang == Russian)
        {
            ui->tbDescAction->setText("Уменьшив норму амортизации, темп роста ресурсов и увеличив сбережение, можно приблизить состояние к состоянию Бангладеша, хотя капиталовооруженность все равно будет медленно падать.");
        }
        else
        {
            ui->tbDescAction->setText("By decreasing amortization quota and resource growth and increasing savings we can make the economy problem more like Bangladesh's, but the capital endowment will slowly decrease.");
        }
    }
}

void MainWindow::on_chbShowChina_toggled(bool checked)
{
    handler->setShowChina(checked);
}

void MainWindow::triggerChange()
{
    switch(lastChanged)
    {
    case Pars::ICE:
        on_sldICE_valueChanged(ui->sldICE->value());
        break;
    case Pars::delta:
        on_sldDelta_valueChanged(ui->sldDelta->value());
        break;
    case Pars::alpha:
        on_sldAlpha_valueChanged(ui->sldAlpha->value());
        break;
    case Pars::s:
        on_sldS_valueChanged(ui->sldS->value());
        break;
    case Pars::n:
        on_sldN_valueChanged(ui->sldN->value());
    }
}

void MainWindow::on_actionEnglish_triggered()
{
    curLang = English;

    ui->lblDelta->setText("Amortization quota (δ)");
    ui->lblIAlpha->setText("Total factor productivity");
    ui->lblICE->setText("Initial capital endowment");
    ui->lblN->setText("Resource growth (n)");
    ui->lblS->setText("Saving ratio (s)");

    ui->gbExamples->setTitle("Examples");
    ui->gbExercise->setTitle("Exercise");
    ui->gbParameters->setTitle("Model parameters");

    ui->actionAgriculture->setText("Agriculture");
    ui->actionIndustry->setText("Industry");
    ui->actionTourism->setText("Tourism");
    ui->actionModel_description->setText("Model description");

    ui->btnBegin->setText("Begin");
    ui->btnEnd->setText("End");
    ui->chbShowChina->setText("Show China");
    ui->tbDescription->setHtml("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\"><html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; font-weight:400; font-style:normal;\"><p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:115%;\"><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\">The model describes how capital endowment of </span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt; color:#0000ff;\">Myanmar</span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\"> agriculture, industry and tourism depends on parameters like resource growth and GDP. Also you can see the changing of capital endowment of </span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt; color:#008000;\">Bangladesh</span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\"> and </span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt; color:#ffa500;\">China</span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\">. The vertical axis shows the capital endowment in millions of dollars, the horizontal one shows the time in years.</span></p></body></html>");

    handler->setChartTitle("Capital endowment model");

    ui->menu->setTitle("Model description");
    ui->menuLanguage->setTitle("Language");
    ui->menuModels->setTitle("Models");

    triggerChange();
}

void MainWindow::on_actionRussian_triggered()
{
    curLang = Russian;

    ui->lblDelta->setText("Норма амортизации (δ)");
    ui->lblIAlpha->setText("Полная факторная производительность");
    ui->lblICE->setText("Начальная капиталовооруженность");
    ui->lblN->setText("Темп роста ресурсов (n)");
    ui->lblS->setText("Норма сбережения (s)");

    ui->gbExamples->setTitle("Примеры");
    ui->gbExercise->setTitle("Упражнение");
    ui->gbParameters->setTitle("Параметры модели");

    ui->btnBegin->setText("Начать");
    ui->btnEnd->setText("Завершить");

    ui->actionAgriculture->setText("Сельское хозяйство");
    ui->actionIndustry->setText("Промышленность");
    ui->actionTourism->setText("Туризм");
    ui->actionModel_description->setText("Описание модели");

    ui->chbShowChina->setText("Показать Китай");
    ui->tbDescription->setHtml("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd\"><html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; font-weight:400; font-style:normal;\"><p style=\" margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:115%;\"><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\">Данная модель описывает капиталовооруженность сельского хозяйства/промышленности </span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt; color:#0000ff;\">Мьянмы</span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\"> в зависимости от таких параметров, как рост населения и ВВП. На графике, для сравнения, можно также наблюдать изменение капиталовооруженности </span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt; color:#008000;\">Бангладеша</span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\"> и </span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt; color:#ffa500;\">Китая</span><span style=\" font-family:'Times New Roman, serif'; font-size:12pt;\"> с течением времени. Вертикальная ось показывает капиталовооруженность в млн. долларов, горизонтальная - время в годах.</span></p></body></html>");

    handler->setChartTitle("Модель накопления капитала");

    ui->menu->setTitle("Описание модели");
    ui->menuLanguage->setTitle("Язык");
    ui->menuModels->setTitle("Модели");

    triggerChange();
}

void MainWindow::on_actionAgriculture_triggered()
{
    ui->sldAlpha->setValue(exAgriculture->getValue(enMyanmar).getValue(alpha) * 10);
    ui->sldDelta->setValue(exAgriculture->getValue(enMyanmar).getValue(delta) * 10);
    ui->sldN->setValue(exAgriculture->getValue(enMyanmar).getValue(n) * 10);
    ui->sldS->setValue(exAgriculture->getValue(enMyanmar).getValue(s) * 10);
    ui->sldICE->setValue(exAgriculture->getValue(enMyanmar).getValue(ice));

    handler->loadExample(*exAgriculture);
}

void MainWindow::on_actionIndustry_triggered()
{
    ui->sldAlpha->setValue(exIndustry->getValue(enMyanmar).getValue(alpha) * 10);
    ui->sldDelta->setValue(exIndustry->getValue(enMyanmar).getValue(delta) * 10);
    ui->sldN->setValue(exIndustry->getValue(enMyanmar).getValue(n) * 10);
    ui->sldS->setValue(exIndustry->getValue(enMyanmar).getValue(s) * 10);
    ui->sldICE->setValue(exIndustry->getValue(enMyanmar).getValue(ice));

    handler->loadExample(*exIndustry);
}

void MainWindow::on_actionTourism_triggered()
{
    ui->sldAlpha->setValue(exTourism->getValue(enMyanmar).getValue(alpha) * 10);
    ui->sldDelta->setValue(exTourism->getValue(enMyanmar).getValue(delta) * 10);
    ui->sldN->setValue(exTourism->getValue(enMyanmar).getValue(n) * 10);
    ui->sldS->setValue(exTourism->getValue(enMyanmar).getValue(s) * 10);
    ui->sldICE->setValue(exTourism->getValue(enMyanmar).getValue(ice));

    handler->loadExample(*exTourism);
}

void MainWindow::on_btnBegin_clicked()
{
    ui->sldAlpha->setValue(exAgriculture->getValue(enMyanmar).getValue(alpha) * 10);
    ui->sldDelta->setValue(exAgriculture->getValue(enMyanmar).getValue(delta) * 10);
    ui->sldN->setValue(exAgriculture->getValue(enMyanmar).getValue(n) * 10);
    ui->sldS->setValue(exAgriculture->getValue(enMyanmar).getValue(s) * 10);
    ui->sldICE->setValue(exAgriculture->getValue(enMyanmar).getValue(ice));

    handler->loadExample(*exAgriculture);

    ui->sldAlpha->setEnabled(false);
    ui->sldDelta->setEnabled(false);
    ui->sldICE->setEnabled(false);
    ui->gbExamples->setEnabled(false);

    exercise = true;
    handler->setExercise(exercise);

    if(curLang == Russian)
    {
        ui->tbDescAction->setText("Настройте параметры так, чтобы через 50 лет капиталовооруженность в сельском хозяйстве стала больше 10 млн. долларов.");
    }
    else
    {
        ui->tbDescAction->setText("Set the parameters so capital endowment in agriculture would be more than 10 million dollars in 50 years.");
    }
}

void MainWindow::on_btnEnd_clicked()
{
    exercise = false;
    handler->setExercise(exercise);

    ui->sldAlpha->setEnabled(true);
    ui->sldDelta->setEnabled(true);
    ui->sldICE->setEnabled(true);
    ui->gbExamples->setEnabled(true);

    ui->tbDescAction->setText("");
}
